Sample Kafka Client
===================

Sample Kafka client using either
[Kafka Streams](https://kafka.apache.org/documentation/streams/) or
[Spark Structured Streaming](https://spark.apache.org/docs/latest/structured-streaming-programming-guide.html)
to transform one topic into another.

