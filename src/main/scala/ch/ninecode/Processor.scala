package ch.ninecode

import java.io.IOException

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Processor
{
    val log: Logger = LoggerFactory.getLogger (getClass)

    // % of keywords in text to accept it
    val THRESHOLD = 0.20

    /**
     * List of valuable keywords.
     */
    var keywords: Set[String] = Set[String]()

    lazy val mapper: ObjectMapper = new ObjectMapper()

    @throws[IOException]
    def getJsonNodeFromStringContent (content: String): JsonNode = mapper.readTree(content)

    def isKeyWord (string: String): Boolean =
    {
        val small = string.toLowerCase.replaceAll ("\\p{Punct}", "")
        ("" != small) && keywords.contains (small)
    }

    def nonNumber(str: String): Boolean =
    {
        try
        {
            val _ = str.toInt
            false
        }
        catch
        {
            case _: NumberFormatException => true
        }
    }

    def multiCharacter (str: String): Boolean = str.length > 1

    def splitWords (str: String): Array[String] = str.split ("[ »«\"“”‘’©®™…•]").filter(nonNumber).filter(multiCharacter)

    def msgExtractor (value: String): Option[String] =
    {
        val raw: JsonNode = getJsonNodeFromStringContent(value)
        val payload: JsonNode = raw.at ("/payload/value")

        val json: JsonNode = getJsonNodeFromStringContent (payload.asText())
        if (!json.at ("/account/bot").asBoolean(false))
        {
            val acct = json.at ("/account/acct").asText ()
            val message = json.at ("/content").asText ()
            val doc: Document = Jsoup.parseBodyFragment (message)
            val contents: String = doc.text ()
            val words: Array[String] = splitWords (contents)
            val keys = words.filter (isKeyWord).toSet.toList
            if ((keys.size.toDouble / words.length.toDouble) > THRESHOLD)
                Some (s"$acct: $contents")
            else
                None
        }
        else
            None
    }

}
