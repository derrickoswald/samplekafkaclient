package ch.ninecode

import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.util.concurrent.CountDownLatch

import scala.io.Source
import scala.tools.nsc.io.Jar
import scala.util.Random

import org.apache.log4j.LogManager
import org.apache.spark._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.udf
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Reads from in_topic and writes to out_topic only messages over THRESHOLD words in corpus from file.
 */
object SparkStreamingMain
{
    LogManager.getLogger(getClass.getName).setLevel(org.apache.log4j.Level.INFO)
    val log: Logger = LoggerFactory.getLogger (getClass)

    def using[T <: AutoCloseable, R] (resource: T)(block: T => R): R =
    {
        try
        {
            block(resource)
        }
        finally
        {
            resource.close()
        }
    }

    // get the containing jar name for an object
    def jarForClass (cls: Class[_]): String =
    {
        var ret = cls.getProtectionDomain.getCodeSource.getLocation.getPath
        try
        {
            ret = URLDecoder.decode(ret, "UTF-8")
        }
        catch
        {
            case e: UnsupportedEncodingException => e.printStackTrace()
        }
        if (!ret.toLowerCase.endsWith(".jar"))
        {
            // as an aid to debugging, make a jar in /tmp and return that name
            val name = s"/tmp/${Random.nextInt(99999999)}.jar"
            val writer = new Jar(new scala.reflect.io.File(new java.io.File(name))).jarWriter()
            writer.addDirectory(new scala.reflect.io.Directory(new java.io.File(s"${ret}ch/")), "ch/")
            writer.close()
            ret = name
        }

        ret
    }

    val processor = new Processor

    def process (message: String): String =
    {
        processor.msgExtractor(message) match
        {
            case Some(string) => string
            case None => ""
        }
    }

    val convert: UserDefinedFunction = udf((x: String) => process(x))

    /**
     * Run with:
     *   $ spark-submit --master local[*] target/SampleKafkaClient-1.0-jar-with-dependencies.jar localhost:9092 mastodon keyword www_cbc_ca.txt
     * @param args program arguments
     */
    def main (args: Array[String]): Unit =
    {
        if (args.length == 4)
        {
            val Array (kafka, in_topic, out_topic, keywords_file) = args
            using (Source.fromFile(keywords_file, "UTF-8"))(
                source =>
                {
                    processor.keywords = processor.keywords ++ source.getLines.toSet
                }
            )

            val configuration = new SparkConf()
                .setAppName("SampleKafkaClient")
            sys.props.get("MASTER") match
            {
                case Some(master) => configuration.setMaster(master)
                case None =>
            }
            val session = SparkSession.builder().config(configuration).getOrCreate()
            session.sparkContext.setLogLevel("WARN")
            session.udf.register("convert", convert)

            import session.implicits._
            val dataframe = session
                .readStream
                .format("kafka")
                .option("kafka.bootstrap.servers", kafka)
                .option("subscribe", in_topic)
                .load()
                .selectExpr("cast(key as string)", "cast(value as string)")
                .as[(String, String)]

            val output = dataframe.selectExpr("key", "convert(value) as value").filter("value <> ''")

            output.writeStream
                .format("kafka")
                .option("kafka.bootstrap.servers", kafka)
                .option("topic", out_topic)
                .option("checkpointLocation", "hdfs://localhost:8020/tmp") // ToDo: configuration somehow
                .start()

            val latch = new CountDownLatch (1)

            // attach shutdown handler to catch control-c
            sys.ShutdownHookThread
            {
                log.info ("shutting down")
                latch.countDown ()
            }

            try
            {
                latch.await ()
            }
            catch
            {
                case e: Throwable =>
                    log.info (e.getLocalizedMessage)
                    System.exit (1)
            }
        }
        else
            log.error (
"""usage: spark-submit SampleKafkaClient host:port in_topic out_topic keywords_file
   where host:port is the Kafka seed node (localhost:9092)
         in_topic is the topic to read (mastodon)
         out_topic is the topic to write to (keywords)
         keywords_file is the list of keywords to match (e.g. www_cbc_ca.txt)
""")
    }
}
