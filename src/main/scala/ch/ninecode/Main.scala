package ch.ninecode

/**
 * Sample Kafka client using either Kafka Streams or Spark Structured Streaming.
 *
 * Assumes default Kafka server startup:
 *   $ bin/zookeeper-server-start.sh config/zookeeper.properties
 *   $ bin/kafka-server-start.sh config/server.properties
 * Uses events posted to in_topic, such as from Kafka Connect plugin kafka-connect-http https://github.com/castorm/kafka-connect-http
 *   $ bin/connect-standalone.sh config/connect-standalone.properties plugins/mastodon.properties
 * where mastodon.properties may contain:
 *   name=mastodon.http.source
 *   connector.class=com.github.castorm.kafka.connect.http.HttpSourceConnector
 *   http.client.read.timeout.millis=10000
 *   http.offset.initial=timestamp=2021-01-05T09:18:17Z
 *   http.request.url=https://cybre.space/api/v1/timelines/public/
 *   http.request.params=
 *   http.response.record.offset.pointer=key=/id,timestamp=/created_at
 *   http.timer.interval.millis=30000
 *   http.response.record.key.pointer=/id
 *   kafka.topic=mastodon
 * To build:
 *   mvn clean install
 */
object Main
{
    /**
     * Main entry point.
     *
     * For running command line samples, see individual main() methods.
     * For debugging Spark Streaming, set jvm options: -DSPARK_SUBMIT=true -DMASTER=local[*]
     * add also include "provided" scope jars on the classpath.
     *
     * @param args program arguments
     */
    def main (args: Array[String]): Unit =
    {
        if (sys.props.getOrElse("SPARK_SUBMIT", "false") == "true")
            SparkStreamingMain.main(args)
        else
            KafkaStreamsMain.main(args)
    }
}
