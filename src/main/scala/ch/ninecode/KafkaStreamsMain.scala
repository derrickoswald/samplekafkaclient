package ch.ninecode

import java.net.URI
import java.util.Properties
import java.util.concurrent.CountDownLatch

import scala.collection.JavaConverters._

import org.apache.kafka.common.utils.Utils.sleep
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream.KStream
import org.apache.kafka.streams.scala.serialization.Serdes
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Reads from in_topic and writes to out_topic only messages over THRESHOLD words in corpus crawled/scraped.
 */
object KafkaStreamsMain
{
    import Serdes._

    val log: Logger = LoggerFactory.getLogger (getClass)

    val processor = new Processor

    val stopwords: Array[String] = Array[String] (
        "a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone",
        "along", "already", "also", "although", "always", "am", "among", "amongst", "amount",  "an", "and",
        "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "around", "as",  "at", "back", "be",
        "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below",
        "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant",
        "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during",
        "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever",
        "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fifty", "fill", "find", "fire",
        "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further",
        "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby",
        "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in",
        "inc", "indeed", "interest", "into", "i", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least",
        "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most",
        "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next",
        "nine", "no", "nobody", "none", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on",
        "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over",
        "own", "part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming",
        "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so",
        "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system",
        "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter",
        "thereby", "therefore", "therein", "thereupon", "these", "they", "thick", "thin", "third", "this", "those",
        "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards",
        "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well",
        "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby",
        "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom",
        "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself",
        "yourselves", "the")

    /**
     * Set of urls to follow to gather keywords.
     */
    var urls: Set[String] = Set[String]()

    /**
     * Set of already followed urls when gathering keywords.
     */
    var followed: Set[String] = Set[String]()

    def next: String =
    {
        val url: String = urls.head
        urls = urls.drop(1)
        followed = followed.+(url)
        url
    }

    def isRealWord (string: String): Boolean =
    {
        val small = string.toLowerCase.replaceAll ("\\p{Punct}", "")
        ("" != small) && !stopwords.contains (small) // ToDo: could be faster with a tree
    }

    def isNew (url: String): Boolean =
    {
        !followed.contains(url) && !urls.contains(url)
    }

    def normalizeLink (raw: String): String =
    {
        try
        {
            val uri = new URI (raw)
            val path = uri.getPath
            s"${uri.getScheme}://${uri.getAuthority}$path${if (path.endsWith ("/")) "" else "/"}"
        }
        catch
        {
            case _: Throwable =>
                raw
        }
    }

    /**
     * Navigate a website and it's links to extract all keywords.
     * @return url the site to crawl
     * @return <code>true</code> if there are still more links to follow
     */
    def crawl (url: String): Boolean =
    {
        if (urls.isEmpty)
            false
        else
        {
            val target = s"$url${if (url.endsWith ("/")) "" else "/"}"
            val current = next
            log.info (s"crawling $current")
            try
            {
                val doc: Document = Jsoup.connect (current).get
                val contents: String = doc.text()
                val words: Array[String] = processor.splitWords(contents)
                val keys: Set[String] = words.filter (isRealWord).map (_.toLowerCase.replaceAll ("\\p{Punct}", "")).toSet
                processor.keywords = processor.keywords ++ keys
                val links: Elements = doc.select ("a[href]")
                val tobefollowed: Iterator[String] = for (
                    link <- links.iterator.asScala;
                    raw = link.attr ("abs:href");
                    candidate = normalizeLink(raw)
                    if !raw.endsWith (".pdf") // ignore pdf files because JSoup bails, ToDo: need something more generic
                    if candidate.startsWith (target)
                    if isNew (candidate))
                    yield candidate
                urls = urls ++ tobefollowed.toSet
                true
            }
            catch
            {
                case e: Throwable =>
                    log.info (s"$current ${e.getLocalizedMessage}")
                    true
            }
        }
    }

    /**
     * Run with:
     *   $ java -jar target/SampleKafkaClient-1.0-jar-with-dependencies.jar localhost:9092 mastodon keyword https://www.cbc.ca/
     * @param args program arguments
     */
    def main (args: Array[String]): Unit =
    {
        if (args.length == 4)
        {
            val Array(kafka, in_topic, out_topic, url) = args
            val props = new Properties ()
            props.put (StreamsConfig.APPLICATION_ID_CONFIG, "SampleKafkaClient")
            props.put (StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafka)
            props.put (StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.stringSerde.getClass)
            props.put (StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.stringSerde.getClass)

            val builder: StreamsBuilder = new StreamsBuilder
            val source: KStream[String,String] = builder.stream[String,String] (in_topic)
            val messages: KStream[String, String] = source.flatMapValues(processor.msgExtractor(_))
            messages.to (out_topic)

            val topology: Topology = builder.build ()
            log.info (topology.describe.toString)
            val streams = new KafkaStreams (topology, props)

            val latch = new CountDownLatch (1)

            // attach shutdown handler to catch control-c
            sys.ShutdownHookThread
            {
                log.info ("shutting down")
                streams.close ()
                latch.countDown ()
            }

            try
            {
                streams.start ()
                // seed the initial url
                urls = urls.+ (url)
                while (crawl (url))
                    sleep (10000)
                log.info (s"finished crawling $url")
                processor.keywords.foreach(println)
                latch.await ()
            }
            catch
            {
                case e: Throwable =>
                    log.info (e.getLocalizedMessage)
                    System.exit (1)
            }
        }
        else
            log.error (
"""usage: java -jar SampleKafkaClient host:port in_topic out_topic url
   where host:port is the Kafka seed node (localhost:9092)
         in_topic is the topic to read (mastodon)
         out_topic is the topic to write to (keyword)
         url is the web site to crawl/scrape for keywords (e.g. https://www.cbc.ca/)
""")

    }
}
